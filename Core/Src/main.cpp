/*
 * Copyright (C) 2018 Arm Limited or its affiliates. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * Description: End-to-end example code for running keyword spotting on 
 * STM32F746NG development kit (DISCO_F746NG in mbed-cli). The example is 
 * derived from https://os.mbed.com/teams/ST/code/DISCO-F746NG_AUDIO_demo
 */

#include "kws_f746ng.h"
#include "plot_utils.h"
#include "LCD_DISCO_F746NG.h"
#include "stm32746g_discovery.h"

#include "stdio.h"

#include "stm32746g_discovery_audio.h"

#include "um.h"

LCD_DISCO_F746NG lcd;
//Serial pc(USBTX, USBRX);
KWS_F746NG *kws;
//Timer T;

char lcd_output_string[64];
const char output_class[12][8] = {"Silence", "Unknown","yes","no","up","down",
                            "left","right","on","off","stop","go"};

typedef enum {
	CLASS_SILENCE,	/* 0 */
	CLASS_UNKNOWN,	/* 1 */
	CLASS_YES,		/* 2*/
	CLASS_NO,		/* 3 */
	CLASS_UP,		/* 4 */
	CLASS_DOWN,		/* 5 */
	CLASS_LEFT,		/* 6 */
	CLASS_RIGHT,	/* 7 */
	CLASS_ON,		/* 8 */
	CLASS_OFF,		/* 9 */
	CLASS_STOP,		/* 10 */
	CLASS_GO,		/* 11 */
} class_t;

// Tune the following three parameters to improve the detection accuracy
//  and reduce false positives
// Longer averaging window and higher threshold reduce false positives
//  but increase detection latency and reduce true positive detections.

// (recording_win*frame_shift) is the actual recording window size
int recording_win = 3;
// Averaging window for smoothing out the output predictions
int averaging_window_len = 3;
int detection_threshold = 90;  //in percent

void run_kws();

volatile uint32_t AudioInFlag;
volatile uint32_t AudioOutFlag;

#define TEST_N	1

int main()
{
#if (TEST_N == 0)
//	arm_barycenter_f32 960

//  pc.baud(9600);


//	uint16_t buffer[1024];
//	for (uint32_t i = 0; i < sizeof(buffer)/sizeof(uint16_t); i++) {
//		buffer[i] = 0;
//	}
//	BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_HEADPHONE, 50, 16000);
//	BSP_AUDIO_OUT_Play(buffer, 128);

  kws = new KWS_F746NG(recording_win,averaging_window_len);
  init_plot();
  kws->start_kws();

//  T.start();

  BSP_LED_Init(LED_GREEN);

  while (1) {
  /* A dummy loop to wait for the interrupts. Feature extraction and
     neural network inference are done in the interrupt service routine. */
    __WFI();

//    HAL_Delay(500);
//    BSP_LED_Toggle(LED_GREEN);

    if (AudioInFlag) {
    	AudioInFlag = 0;

//    	for (int i = kws->audio_block_size*2; i < kws->audio_block_size*4; i+=2) {
//    		kws->audio_buffer_in[i+1] = kws->audio_buffer_in[i];
//    	}

    	arm_copy_q7((q7_t *)kws->audio_buffer_in + kws->audio_block_size*4, (q7_t *)kws->audio_buffer_out + kws->audio_block_size*4, kws->audio_block_size*4);
		if(kws->frame_len != kws->frame_shift) {
			//copy the last (frame_len - frame_shift) audio data to the start
			arm_copy_q7((q7_t *)(kws->audio_buffer)+2*(kws->audio_buffer_size-(kws->frame_len-kws->frame_shift)), (q7_t *)kws->audio_buffer, 2*(kws->frame_len-kws->frame_shift));
		}
		// copy the new recording data
		for (int i=0;i<kws->audio_block_size;i++) {
			kws->audio_buffer[kws->frame_len-kws->frame_shift+i] = kws->audio_buffer_in[2*kws->audio_block_size+i*2];
		}
		run_kws();
    }

    if (AudioOutFlag) {
    	AudioOutFlag = 0;

//    	for (int i = kws->audio_block_size*0; i < kws->audio_block_size*2; i+=2) {
//    		kws->audio_buffer_in[i+1] = kws->audio_buffer_in[i];
//    	}

    	arm_copy_q7((q7_t *)kws->audio_buffer_in, (q7_t *)kws->audio_buffer_out, kws->audio_block_size*4);
		if(kws->frame_len!=kws->frame_shift) {
			//copy the last (frame_len - frame_shift) audio data to the start
			arm_copy_q7((q7_t *)(kws->audio_buffer)+2*(kws->audio_buffer_size-(kws->frame_len-kws->frame_shift)), (q7_t *)kws->audio_buffer, 2*(kws->frame_len-kws->frame_shift));
		}
		// copy the new recording data
		for (int i=0;i<kws->audio_block_size;i++) {
				kws->audio_buffer[kws->frame_len-kws->frame_shift+i] = kws->audio_buffer_in[i*2];
		}
		run_kws();
    }

  }

#else
  //int16_t audio_buffer[16000]=WAVE_DATA;
  KWS_DS_CNN kws(audio_buffer);

  kws.extract_features(); //extract mfcc features
  kws.classify();	  //classify using dnn

  int max_ind = kws.get_top_class(kws.output);
  volatile int p = ((int)kws.output[max_ind]*100/128);
  volatile class_t s = (class_t)max_ind;

  (void)p;
  (void)s;

  while(1);

#endif
  return 0;
}


/*
 * The audio recording works with two ping-pong buffers.
 * The data for each window will be tranfered by the DMA, which sends
 * sends an interrupt after the transfer is completed.
 */

// Manages the DMA Transfer complete interrupt.
void BSP_AUDIO_IN_TransferComplete_CallBack(void)
{
	AudioInFlag = 1;
}

// Manages the DMA Half Transfer complete interrupt.
void BSP_AUDIO_IN_HalfTransfer_CallBack(void)
{
	AudioOutFlag = 1;
}

void run_kws()
{
  kws->extract_features();    //extract mfcc features
  kws->classify();	      //classify using dnn
  kws->average_predictions();
//  plot_mfcc();
  plot_waveform();
  int max_ind = kws->get_top_class(kws->averaged_output);
  if(kws->averaged_output[max_ind]>detection_threshold*128/100)
    sprintf(lcd_output_string,"%d%% %s",((int)kws->averaged_output[max_ind]*100/128),output_class[max_ind]);
  lcd.ClearStringLine(8);
  lcd.DisplayStringAt(0, LINE(8), (uint8_t *) lcd_output_string, CENTER_MODE);
}

/**
  * @brief  Clock Config.
  * @param  hsai: might be required to set audio peripheral predivider if any.
  * @param  AudioFreq: Audio frequency used to play the audio stream.
  * @note   This API is called by BSP_AUDIO_OUT_Init() and BSP_AUDIO_OUT_SetFrequency()
  *         Being __weak it can be overwritten by the application
  * @retval None
  */
void BSP_AUDIO_OUT_ClockConfig(SAI_HandleTypeDef *hsai, uint32_t AudioFreq, void *Params)
{
  RCC_PeriphCLKInitTypeDef RCC_ExCLKInitStruct;

  HAL_RCCEx_GetPeriphCLKConfig(&RCC_ExCLKInitStruct);

  /* Set the PLL configuration according to the audio frequency */
  if((AudioFreq == AUDIO_FREQUENCY_11K) || (AudioFreq == AUDIO_FREQUENCY_22K) || (AudioFreq == AUDIO_FREQUENCY_44K))
  {
    /* Configure PLLSAI prescalers */
    /* PLLI2S_VCO: VCO_429M
    SAI_CLK(first level) = PLLI2S_VCO/PLLSAIQ = 429/2 = 214.5 Mhz
    SAI_CLK_x = SAI_CLK(first level)/PLLI2SDivQ = 214.5/19 = 11.289 Mhz */
    RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI2;
    RCC_ExCLKInitStruct.Sai2ClockSelection = RCC_SAI2CLKSOURCE_PLLI2S;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SP = 8;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = 429;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SQ = 2;
    RCC_ExCLKInitStruct.PLLI2SDivQ = 19;
    HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct);
  }
  else /* AUDIO_FREQUENCY_8K, AUDIO_FREQUENCY_16K, AUDIO_FREQUENCY_48K), AUDIO_FREQUENCY_96K */
  {
    /* SAI clock config
    PLLI2S_VCO: VCO_344M
    SAI_CLK(first level) = PLLI2S_VCO/PLLSAIQ = 344/7 = 49.142 Mhz
    SAI_CLK_x = SAI_CLK(first level)/PLLI2SDivQ = 49.142/1 = 49.142 Mhz */
    RCC_ExCLKInitStruct.PeriphClockSelection = RCC_PERIPHCLK_SAI2;
    RCC_ExCLKInitStruct.Sai2ClockSelection = RCC_SAI2CLKSOURCE_PLLI2S;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SP = 8;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SN = 344;
    RCC_ExCLKInitStruct.PLLI2S.PLLI2SQ = 7;
    RCC_ExCLKInitStruct.PLLI2SDivQ = 1;
    HAL_RCCEx_PeriphCLKConfig(&RCC_ExCLKInitStruct);
  }
}
